using Plots
using DelimitedFiles
using Printf

function get_data(file)
    if (isfile(file))
        data = readdlm(file, Float64)
        return data
    end
end

function plot_water_height(file)
    y = get_data(file)
    n = length(y[1,2:end])
    x = 1:n
    anim = @animate for i ∈ 1:10:length(y[:,1])
        plot(x, y[i,2:end], linewidth=3, legend=false)
        title!(@sprintf "Water elevation [m], time step %d" i)
        xlabel!("Spatial grid index")
        ylabel!("Height")
        ylims!(0, 1)
    end
    gif(anim, "water_height.gif", fps = 60)
end

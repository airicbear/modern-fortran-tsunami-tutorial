program tsunami
  ! Step 1:
  ! dh/dt + u * dh/dx = 0
  ! 
  ! Step 2:
  ! du/dt + u * du/dx + g * dh/dx = 0
  ! dh/dt + d(h * u)/dx = 0
  use mod_diff, only: diff => diff_centered

  implicit none

  integer, parameter :: grid_size = 100
  integer, parameter :: num_time_steps = 1000

  real, parameter :: dx = 1.0, dt = 0.02, g = 9.8

  integer :: i, n

  integer, parameter :: icenter = 50
  real, parameter :: decay = 0.01
  real, parameter :: depth = 10

  real :: h(grid_size), u(grid_size)

  u = 0
  init: do i = 1, grid_size
     h(i) = exp(- decay * (i - icenter)**2)
  end do init

  n = 0

  time_loop: do n = 1, num_time_steps

     u = u - (u * diff(u) + g * diff(h)) / dx * dt
     h = h - diff((h + depth) * u) / dx * dt

     print *, n, h

  end do time_loop

end program tsunami
